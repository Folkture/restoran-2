class ElmntFooter extends HTMLElement{
    connectedCallback(){
        this.render();
    }

    render(){
        this.innerHTML = `
            <div class="container">
                
                <div class="text-box">
                    <h1>Pecel Lele</h1>
                    <p class="last">Copyright @pann</p>
                </div>
            </div>
        `;
    }
}

customElements.define('elmnt-footer', ElmntFooter);